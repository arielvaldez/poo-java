// Además, debe ser posible consultar para un préstamo si ya debe ser devuelto
// (en función de la fecha actual del sistema –puede crear un método en dicha clase
//que indique cuántos días restan para devolverlo);
// implemente otro método que renueve el préstamo sólo si no está vencido[2].

// Luego, usando la clase java.io.Console, realice un programa que permita cargar préstamos
// en un arreglo y permita, luego, mostrarlos por salida estándar

import java.io.Console;  // https://docs.oracle.com/javase/8/docs/api/java/io/Console.html
import java.util.ArrayList;  // http://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html#ArrayList()
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class Prestamo {
    private String nombre_socio;
    private GregorianCalendar fecha_prestamo;
    private GregorianCalendar fecha_devolucion;
    private Libro ejemplar_prestado;

    public String toString() {  // como def __unicode__(self): de Python
        return this.nombre_socio + " - " + this.ejemplar_prestado;
    }

    // public long dias_restantes(GregorianCalendar fecha_devolucion) {
    //     Date hoy = new Date();
    //     Date devolucion = fecha_devolucion.getTime();
    //     long ahora_mili = hoy.getTime();
    //     // long devolucion_mili = fecha_devolucion.getTime();
    //     int dias = (int)(devolucion - ahora_mili) / (1000 * 60 * 60 * 24);
    //     return dias;
    // }

    public static void main(String[] args) {

        GregorianCalendar fecha_publicacion = new GregorianCalendar(1997, 5, 05);
        Libro libro = new Libro("Cien años de soledad", "Gabriel García Márquez", fecha_publicacion, "9789580600022", "Harper", 471);

        Console console = System.console();

        Prestamo prestamo = new Prestamo();
        prestamo.nombre_socio = console.readLine("%s", "Nombre de socio: ");
        String fecha_prestamo = console.readLine("%s", "Fecha de préstamo (dd-mm-yy): ");

        String[] lista_fecha = fecha_prestamo.split("-");
        int day = Integer.parseInt(lista_fecha[0]);
        int month = Integer.parseInt(lista_fecha[1]) - 1;
        int year = Integer.parseInt(lista_fecha[2]);

        prestamo.fecha_prestamo = new GregorianCalendar(year, month, day);
        
        String fecha_devolucion = console.readLine("%s", "Fecha de devolución (dd-mm-yy): ");

        lista_fecha = fecha_devolucion.split("-");
        day = Integer.parseInt(lista_fecha[0]);
        month = Integer.parseInt(lista_fecha[1]) - 1;
        year = Integer.parseInt(lista_fecha[2]);

        prestamo.fecha_devolucion = new GregorianCalendar(year, month, day, 0, 0, 0);

        // String ejemplar = console.readLine("%s", "Libro prestado: ");

        prestamo.ejemplar_prestado = libro;

        ArrayList<Prestamo> lista_prestamos = new ArrayList<Prestamo>();

        lista_prestamos.add(prestamo);

        // SimpleDateFormat sdf = new SimpleDateFormat("dd MMMMM yy");
        // String fecha = sdf.format(prestamo.fecha_devolucion.getTime());
        // console.printf(fecha);
        // console.printf("\n");

        for (Prestamo item : lista_prestamos) {
            console.printf(item.toString());
            console.printf("\n");
        }

        // long ahora = prestamo.dias_restantes(prestamo.fecha_devolucion);
        // System.out.println(ahora);

    }
}
