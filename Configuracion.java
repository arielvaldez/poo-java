import java.util.*;
import java.awt.*;
import java.awt.event.*;

public class Configuracion extends Frame implements ActionListener{
    Label pantalla;
    Checkbox cb1,cb2,cb3,cb4;
    CheckboxGroup cbg_pantalla;
    Label sonido;
    CheckboxGroup cbg_sonido;
    Label teclas;
    Label musica;
    Choice ch_musica;
    Button reset;

    public Configuracion(){
        super("Configuracion Kingdom Rush");

        Panel p1 = new Panel(new FlowLayout(FlowLayout.LEFT));

        pantalla = new Label("Pantalla");
        p1.add(pantalla);

        Panel p2 = new Panel();
        cbg_pantalla = new CheckboxGroup();
        cb1 = new Checkbox("Ventana", cbg_pantalla, true);
        cb2 = new Checkbox("Pantalla Completa", cbg_pantalla, false);
        p2.add(cb1);
        p2.add(cb2);

        Panel p3 = new Panel();
        sonido = new Label("Sonido");
        p3.add(sonido);

        Panel p4 = new Panel();
        cbg_sonido = new CheckboxGroup();
        cb3 = new Checkbox ("Activado",cbg_sonido,true);
        cb4 = new Checkbox ("Desactivado",cbg_sonido,false);
        p4.add(cb3);
        p4.add(cb4);

        Panel p5 = new Panel();
        musica = new Label("Musica");
        p5.add(musica);

        Panel p6 = new Panel();
        ch_musica = new Choice();
        ch_musica.add("Tema Original");
        ch_musica.add("La mordidita");
        ch_musica.add("Despacito");
        p6.add(ch_musica);

        reset = new Button("RESET");
        reset.addActionListener(this);

        this.setLayout(new FlowLayout(FlowLayout.LEFT, 1, 1));
        this.add(p1);
        this.add(p2);
        this.add(p3);
        this.add(p4);
        this.add(p5);
        this.add(p6);
        this.add(reset);

        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                  System.exit(0);
            }
        });

        this.pack();
        this.setVisible(true);
    }

    public static void main (String[] args) {
        Configuracion config = new Configuracion();
        config.setSize(250, 210);
        // config.setResizable(false);
    }

    public void actionPerformed(ActionEvent e){
        if (e.getActionCommand()==reset.getActionCommand()){
            cb1.setState(true);
            cb3.setState(true);
            ch_musica.select(0);
        }
    }
}
