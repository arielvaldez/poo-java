public interface Debuggeable {
    public static final int LEVEL_ERROR = 0;
    public static final int LEVEL_WARNING = 1;
    public static final int LEVEL_SUCCESS = 2;

    /** Devolver una cadena que describa el estado interno de todos los atributos
    de la clase
    * @return  
    */
    public String devolverEstado();
}
