// 2.1.3.

// import java.util.regex.Pattern;  // https://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html
// import java.util.regex.Matcher;  // https://docs.oracle.com/javase/8/docs/api/java/util/regex/Matcher.html
// Pattern pattern = Pattern.compile(regex);
import java.util.HashMap;
import java.util.Collections;

public class ContadorDePalabras {

    /**
     * Recibe una expresión regular, un array y devuelve la cantidad de veces
     * que la expresión regular se repite en el array.
     * @param regex Expresión regular para realizar el análisis.
     * @param cadena Array de palabras que se analiza.
     * @return Cantidad de ocurrencias de la expresión regular en la cadena.
     */
    private static int cantidadCaracteres(String regex, String cadena) {
        int cantidad = 0;

        String caracter;

        for (int i = 0; i < cadena.length(); i++) {
            // caracter = String.valueOf(cadena.charAt(i));
            // obtiene caracter por caracter y lo convierte a string nuevamente
            caracter = Character.toString(cadena.charAt(i));

            if (caracter.matches(regex)) {
                cantidad += 1;
            }
        }

        return cantidad;
    }

    public static void main(String[] arg) {
        // guarda en un array las palabras, el split lo hace sobre un espacio o más
        int palabras = 0;
        String[] lista_cadena = arg[0].split("\\s+");

        for (int i=0; i < lista_cadena.length; i++) {
            if (!lista_cadena[i].equals("")) {
                palabras += 1;
            }
        }

        // guarda la cadena pasada por paŕametro
        String cadena = arg[0];

        // cadena_sin_espacios   => todas las palabras juntas
        // cadena.length()       => longitud total de la cadena
        String cadena_sin_espacios = cadena.replaceAll(" ", "");
        int espacios = cadena.length() - cadena_sin_espacios.length();

        int a = cadena_sin_espacios.replaceAll("[^a]", "").length();
        int e = cadena_sin_espacios.replaceAll("[^e]", "").length();
        int i = cadena_sin_espacios.replaceAll("[^i]", "").length();
        int o = cadena_sin_espacios.replaceAll("[^o]", "").length();
        int u = cadena_sin_espacios.replaceAll("[^u]", "").length();

        HashMap<String, Integer> map = new HashMap<String, Integer>();
        map.put("a", a);
        map.put("e", e);
        map.put("i", i);
        map.put("o", o);
        map.put("u", u);

        // Devuelve el máximo valor en el HashMap
        // int vocal_mas_veces = Collections.max(map.values());
        String vocal_mas_veces = Collections.max(map.entrySet(), HashMap.Entry.comparingByValue()).getKey();

        int caracteres_numericos = cantidadCaracteres("\\d+", cadena_sin_espacios);
        int vocales = cantidadCaracteres("[aeiou]", cadena_sin_espacios);

        System.out.println("Cant. de palabras: " + palabras);
        System.out.println("Cant. de vocales: " + vocales);
        System.out.println("Vocal con más frecuencia: " + vocal_mas_veces);
        System.out.println("Cant. de espacios: " + espacios);
        System.out.println("Cant. de caracteres númericos: " + caracteres_numericos);
    }
}


