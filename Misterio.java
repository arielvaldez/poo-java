import java.awt.*;
import java.util.Vector;
import java.awt.event.*;

public class Misterio extends Frame implements ActionListener {
    Button biniciar;
    Button bok;
    Button bfin;
    Button bCancel;
    TextArea text;
    TextField valor;
    Panel pnros;
    Vector<Button> bn;
    Cuenta c;

    public Misterio() {
        super("Misterio");
        Panel panel = new Panel();
        panel.setLayout(new BorderLayout());
        panel.setFont(new Font("SansSerif", Font.PLAIN, 30));
        biniciar = new Button("Iniciar Sesión");
        bfin = new Button("Finalizar Sesión");
        text = new TextArea("", 4, 30, TextArea.SCROLLBARS_VERTICAL_ONLY);
        text.setEditable(false);
        bn = new Vector<Button>();
        for (int i=0; i<10; i++) {
            Button b = new Button(Integer.toString(i));
            b.addActionListener(this);
            bn.add(b);
        }
        valor = new TextField(5);
        valor.setEditable(false);
        bok = new Button("OK");
        bCancel = new Button("X");

        Panel pcontroles = new Panel();
        pcontroles.setLayout(new BorderLayout());

        pnros = new Panel();
        pnros.setLayout(new GridLayout(4, 3));
        pnros.setEnabled(false);

        panel.add(text, BorderLayout.CENTER);
        panel.add(pcontroles, BorderLayout.EAST);

        pcontroles.add(valor, BorderLayout.NORTH);
        pcontroles.add(pnros, BorderLayout.CENTER);

        for (Button b:bn) {
            pnros.add(b);
        }

        pnros.add(bok);
        pnros.add(bCancel);

        bok.addActionListener(this);
        bCancel.addActionListener(this);
        biniciar.addActionListener(this);
        bfin.addActionListener(this);

        this.add(panel, BorderLayout.CENTER);
        this.add(biniciar, BorderLayout.NORTH);
        this.add(bfin, BorderLayout.SOUTH);

        WindowListener listener = new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        };

        this.pack();
        this.setVisible(true);

    }   // Ends Constructor

    public void actionPerformed(ActionEvent e) {
        if ("0123456789".indexOf(e.getActionCommand()) >= 0) valor.setText(valor.getText() + e.getActionCommand());

        if (e.getSource().equals(bCancel)) valor.setText("");

        if (e.getSource().equals(bok)) {

            if (c.listo()) {
                text.setText(c.setOperacion(Integer.parseInt(valor.getText().substring(0,1))) + "\n");
            } else {
                c.ejecutar(Double.parseDouble(valor.getText()));
            }

            if (c.listo()) {
                text.append(c.mostrarOpciones() + "\n");
            }
            valor.setText("");
        }

        if (e.getSource().equals(biniciar)) {
            c = new Cuenta(200);
            pnros.setEnabled(true);
            biniciar.setEnabled(false);
            text.setText(c.mostrarOpciones() + "\n");
        }

        if (e.getSource().equals(bfin)) {
            System.exit(0);
        }
    }

    public static void main (String a[]) {
        Misterio atm = new Misterio();
    }

}  // End Class

class Cuenta {
    private double saldo;
    public static final int NINGUNA=-1;
    public static final int CONSULTAR=0;
    public static final int DEPOSITAR=1;
    public static final int EXTRAER=2;
    private int ope = NINGUNA;

    public Cuenta(double s) {
        saldo = s;
    }

    public void ejecutar(double valor) {
        switch (ope) {
            case EXTRAER: saldo = saldo - valor; break;
            case DEPOSITAR: saldo = saldo + valor; break;
        }
        ope = NINGUNA;
    }

    public Boolean listo() {
        return ope==NINGUNA;
    }

    public String mostrarOpciones() {
        return "==== \n 0 - CONSULTAR, 1 - DEPOSITAR, 2 - EXTRAER";
    }

    public String setOperacion(int o) {
        String msg = "";
        ope = o;
        switch(ope) {
            case CONSULTAR: msg = "Su saldo es " + saldo; ope = NINGUNA; break;
            case DEPOSITAR: msg = "Ingrese el monto a depositar"; break;
            case EXTRAER: msg = "Ingrese el monto a extraer"; break;
            default: msg = "Opción incorrecta"; ope = NINGUNA;
        }
        return msg;
    }

}
