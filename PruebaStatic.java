public class PruebaStatic {

    public static int miembroClase = 0;    
    public int miembroObjeto = 1;
    public final int miembroFinal = 3;

    public static void main(String args[]) {
        System.out.println("Hola desde main");
        mostrarMensajeBienvenida();
        // mostrarMensajeSinStatic();  // descomentar para ver que no anda

        PruebaStatic objeto = new PruebaStatic();
        objeto.mostrarMensajeSinStatic();
        // también se puede llamar un método static desde un objeto, pero no se aconseja
        // porque parece que nos estamos refiriendo a un miembro de un objeto en particular
        // objeto.mostrarMensajeBienvenida();
    }

    public static void mostrarMensajeBienvenida() {
        System.out.println("Hola desde método de clase");
        System.out.println(miembroClase + "\n");
        // System.out.println(miembroObjeto + "\n");  // variables no-static NO pueden referenciarse desde un método static
    }

    public void mostrarMensajeSinStatic() {
        System.out.println("Este mensaje es llamado desde un objeto");  // necesita un objeto de la clase para se usado
        System.out.println("Miembro de clase: " + miembroClase + "\n");
        miembroObjeto += 1;
        System.out.println("Miembro de objeto: " + miembroObjeto + "\n");
        // miembroFinal += 1;  // no se puede; es un variable final
        System.out.println("Miembro final: " + miembroFinal + "\n");
    }
}
