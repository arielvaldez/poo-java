// 2.3.2
// tutorial de ayuda http://chuidiang.org/java/layout/GridBagLayout/GridBagLayout.php

import java.awt.Button;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.Frame;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.Panel;
import java.awt.TextField;


public class Calculadora extends Panel implements ActionListener {

    private Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn0, btnDiv, btnMul, btnSum, btnRes, btnComa, btnIgual, btnCE, btnC, btnBorrar;
    private TextField resultTextField;
    String allText;
    double lastInput = 0;
    boolean actionButton = false; // para saber si el último boton presionado no fue un número

    public Calculadora(Panel panel) {

        panel.setFont(new Font("SansSerif", Font.PLAIN, 30));

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(2, 2, 2, 2);
        constraints.weightx = 1;
        constraints.weighty = 1;

        resultTextField = new TextField("0");
        constraints.gridx = 0; // El área de texto empieza en la columna cero.
        constraints.gridy = 0; // El área de texto empieza en la fila cero
        constraints.gridwidth = 4; // El área de texto ocupa cuatro columnas.
        constraints.gridheight = 1; // El área de texto ocupa 1 fila.
        constraints.fill = GridBagConstraints.HORIZONTAL;
        panel.add(resultTextField, constraints);

        btnBorrar = new Button("<-");
        constraints.gridx = 4;
        constraints.gridy = 0;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        constraints.fill = GridBagConstraints.BOTH;
        panel.add(btnBorrar, constraints);
        btnBorrar.addActionListener(this);

        btn7 = new Button("7");
        btn7.setPreferredSize(new Dimension(70, 70));
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        panel.add(btn7, constraints);
        btn7.addActionListener(this);

        btn8 = new Button("8");
        btn8.setPreferredSize(new Dimension(70, 70));
        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        panel.add(btn8, constraints);
        btn8.addActionListener(this);

        btn9 = new Button("9");
        btn9.setPreferredSize(new Dimension(70, 70));
        constraints.gridx = 2;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        panel.add(btn9, constraints);
        btn9.addActionListener(this);

        btnDiv = new Button("/");
        btnDiv.setPreferredSize(new Dimension(70, 70));
        constraints.gridx = 3;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        panel.add(btnDiv, constraints);
        btnDiv.addActionListener(this);

        btnCE = new Button("CE");
        btnCE.setPreferredSize(new Dimension(70, 70));
        constraints.gridx = 4;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        panel.add(btnCE, constraints);
        btnCE.addActionListener(this);

        btn4 = new Button("4");
        btn4.setPreferredSize(new Dimension(70, 70));
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        panel.add(btn4, constraints);
        btn4.addActionListener(this);

        btn5 = new Button("5");
        btn5.setPreferredSize(new Dimension(70, 70));
        constraints.gridx = 1;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        panel.add(btn5, constraints);
        btn5.addActionListener(this);

        btn6 = new Button("6");
        btn6.setPreferredSize(new Dimension(70, 70));
        constraints.gridx = 2;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        panel.add(btn6, constraints);
        btn6.addActionListener(this);

        btnMul = new Button("*");
        btnMul.setPreferredSize(new Dimension(70, 70));
        constraints.gridx = 3;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        panel.add(btnMul, constraints);
        btnMul.addActionListener(this);

        btnC = new Button("C");
        btnC.setPreferredSize(new Dimension(70, 70));
        constraints.gridx = 4;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        panel.add(btnC, constraints);
        btnC.addActionListener(this);

        btn1 = new Button("1");
        btn1.setPreferredSize(new Dimension(70, 70));
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        panel.add(btn1, constraints);
        btn1.addActionListener(this);

        btn2 = new Button("2");
        btn2.setPreferredSize(new Dimension(70, 70));
        constraints.gridx = 1;
        constraints.gridy = 3;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        panel.add(btn2, constraints);
        btn2.addActionListener(this);

        btn3 = new Button("3");
        btn3.setPreferredSize(new Dimension(70, 70));
        constraints.gridx = 2;
        constraints.gridy = 3;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        panel.add(btn3, constraints);
        btn3.addActionListener(this);

        btnRes = new Button("-");
        constraints.gridx = 3;
        constraints.gridy = 3;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        panel.add(btnRes, constraints);
        btnRes.addActionListener(this);

        btn0 = new Button("0");
        btn0.setPreferredSize(new Dimension(70, 70));
        constraints.gridx = 0;
        constraints.gridy = 4;
        constraints.gridwidth = 2;
        constraints.gridheight = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        panel.add(btn0, constraints);
        btn0.addActionListener(this);

        btnComa = new Button(",");
        btnComa.setPreferredSize(new Dimension(70, 70));
        constraints.gridx = 2;
        constraints.gridy = 4;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        panel.add(btnComa, constraints);
        btnComa.addActionListener(this);

        btnSum = new Button("+");
        btnSum.setPreferredSize(new Dimension(70, 70));
        constraints.gridx = 3;
        constraints.gridy = 4;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        panel.add(btnSum, constraints);
        btnSum.addActionListener(this);

        btnIgual = new Button("=");
        constraints.gridx = 4;
        constraints.gridy = 3;
        constraints.gridwidth = 1;
        constraints.gridheight = 2;
        constraints.fill = GridBagConstraints.BOTH;
        panel.add(btnIgual, constraints);
        btnIgual.addActionListener(this);

    }

    public static void main(String[] args) {
        Frame myFrame = new Frame("Calculadora");
        // myFrame.setSize(300, 300);
        myFrame.setResizable(false);

        Panel panel = new Panel();
        panel.setLayout(new GridBagLayout());

        Calculadora calculadora = new Calculadora(panel);

        WindowListener listener = new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        };
        myFrame.addWindowListener(listener);

        /* Creo un nuevo panel para poder meter el otro ya creado
        Este sirve para poder añadir margenes al panel interior */
        // Panel margins = new Panel();
        // margins.setLayout(new GridBagLayout());
        // GridBagConstraints constraints = new GridBagConstraints();
        // constraints.insets = new Insets(10, 10, 10, 10);
        // margins.add(panel, constraints);

        // agrego el panel más externo
        myFrame.add(panel);

        myFrame.pack();
        // para centrar la ventana
        myFrame.setLocationRelativeTo(null);
        myFrame.setVisible(true);

    }

    /*private double doCalculations(Vector vector) {
        double result = 0;
        for (Object item : vector) {
            if (item instanceof double) {
                result += 
            }
        }
        return result;
    }*/

    public void actionPerformed(ActionEvent evt) {
        
        allText = resultTextField.getText();
        if (allText.equals("0")) {
            allText = "";
        }

        if (actionButton == true) {
            allText = "";
            actionButton = false;
        }

        if (evt.getActionCommand() == btn1.getActionCommand()) {
            resultTextField.setText(allText + "1");
        }
        if (evt.getActionCommand() == btn2.getActionCommand()) {
            resultTextField.setText(allText + "2");
        }
        if (evt.getActionCommand() == btn3.getActionCommand()) {
            resultTextField.setText(allText + "3");
        }
        if (evt.getActionCommand() == btn4.getActionCommand()) {
            resultTextField.setText(allText + "4");
        }
        if (evt.getActionCommand() == btn5.getActionCommand()) {
            resultTextField.setText(allText + "5");
        }
        if (evt.getActionCommand() == btn6.getActionCommand()) {
            resultTextField.setText(allText + "6");
        }
        if (evt.getActionCommand() == btn7.getActionCommand()) {
            resultTextField.setText(allText + "7");
        }
        if (evt.getActionCommand() == btn8.getActionCommand()) {
            resultTextField.setText(allText + "8");
        }
        if (evt.getActionCommand() == btn9.getActionCommand()) {
            resultTextField.setText(allText + "9");
        }
        if (evt.getActionCommand() == btn0.getActionCommand()) {
            if (!allText.equals("")) {
                resultTextField.setText(allText + "0");
            }
        }

        if (evt.getActionCommand() == btnCE.getActionCommand()) {
            resultTextField.setText(null);
            lastInput = 0;
        }

        if (evt.getActionCommand() == btnComa.getActionCommand()) {
            resultTextField.setText(allText + ",");
        }

        if (evt.getActionCommand() == btnSum.getActionCommand()) {
            if (!allText.equals("")) {
                lastInput += Double.parseDouble(allText.replace(",","."));
                resultTextField.setText(Double.toString(lastInput));
            }/* else {
                lastInput += lastInput;
                resultTextField.setText(Double.toString(lastInput));
            }*/
            actionButton = true;
        }

        if (evt.getActionCommand() == btnRes.getActionCommand()) {
            if (!allText.equals("")) {
                if (lastInput == 0) {
                    lastInput = Double.parseDouble(allText.replace(",","."));
                } else {
                    lastInput -= Double.parseDouble(allText.replace(",","."));
                }
                resultTextField.setText(Double.toString(lastInput));
            }/* else {
                lastInput -= lastInput;
                resultTextField.setText(Double.toString(lastInput));
            }*/
            actionButton = true;
        }
        if (evt.getActionCommand() == btnIgual.getActionCommand()) {

        }
    }
}
