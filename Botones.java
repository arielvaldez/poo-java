// 2.3.1

import java.awt.*;
import java.awt.event.*;

public class Botones extends Panel implements ActionListener {

    private Button b1, b2, b3;
    private TextField btn1TextField, btn2TextField;
    private TextArea btn3TextArea;

    public Botones() {
        b1 = new Button("Boton B1");
        b2 = new Button("Boton B2");
        b3 = new Button("Boton B3");
        btn1TextField = new TextField(30);
        btn2TextField = new TextField(30);
        btn3TextArea = new TextArea(10, 10);

        this.setLayout(new GridLayout(2,3));

        this.add(b1);
        this.add(b2);
        this.add(b3);
        this.add(btn1TextField);
        this.add(btn2TextField);
        this.add(btn3TextArea);

        b1.addActionListener(this);
        b2.addActionListener(this);
        b3.addActionListener(this);
    }

    public static void main(String[] args) {
        // Crea una nueva ventana
        Frame myFrame = new Frame("Botones Nuevo");

        // Clase anonima
        WindowListener l = new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            };
        };
        myFrame.addWindowListener(l);

        // Crea una instancia de Botones
        Botones b = new Botones();
        // Agrega el objeto para que se muestre por la ventana
        myFrame.add("Center", b);
        // Redimensiona la ventana a su tamanio natural
        myFrame.pack();
        myFrame.setVisible(true);
    }

    public void actionPerformed(ActionEvent evt) {
        if (evt.getActionCommand() == b1.getActionCommand())
            btn1TextField.setText("Se ha pulsado el boton B1");

        if (evt.getActionCommand() == b2.getActionCommand())
            btn2TextField.setText("Se ha pulsado el boton B2");

        if (evt.getActionCommand() == b3.getActionCommand())
            btn3TextArea.append("Se ha pulsado el boton B3\n");
    }
}
