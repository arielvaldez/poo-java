public class Colores implements Debuggeable {
    // atributo estático
    public final static int ROJO = 1;  // pertenece a la clase
    // con final digo que la variable es inmutable (constante)
    private int unColor;

    public Colores() {
        unColor = ROJO;
    }

    public void algo() {
        unColor = ROJO;
        System.out.println(ROJO);
    }

    public String devolverEstado() {
        return "un color " + unColor;
    }


    // metodo estatático
    public static void algoEstatico(Colores c) {  // es para no tener que instanciar objetos, no hace falta guardar
        System.out.println(c.unColor);
    }

    public static void main (String args[]) {
        // Colores color1 = new Colores();

        System.out.println(Colores.ROJO);
        // System.out.println(color1.ROJO);
        // color1.algo();
    }

    // public abstract void algoAbstracto();

}
