public class Punto3D extends Punto2D {
    private int z;

    public Punto3D() {
        super(2, 2);
        System.out.println("Se creó un nuevo Punto3D " + getX());
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public Punto3D(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }

    public static void main (String args[]) {
        java.util.Vector<Debuggeable> vec = new java.util.Vector<Debuggeable>();
        vec.add(new Colores());

        Debuggeable p3d = new Punto3D(3, 4, 5);
        vec.add(p3d);
        System.out.println(Debuggeable.LEVEL_ERROR);
        // vec.add("una cadena");

        // System.out.println(p3d.devolverEstado());

        for (Debuggeable item: vec) {
            System.out.println(item.devolverEstado());
        }
    }
}
