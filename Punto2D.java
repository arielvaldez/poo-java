// POJO: Plani Old Java Object

public class Punto2D implements Debuggeable {
    private int x,y;

    public static int cat = 0;

    public Punto2D() {
        System.out.println("Se creó un nuevo Punto2D");
        x = 1;
        y = 1;
    }

    public Punto2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public String devolverEstado() {
        return "x=" + x + "; y=" + y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

}