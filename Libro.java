import java.util.Vector;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class Libro {

    private String nombre;
    private String autor;
    private GregorianCalendar fecha_publicacion;
    private String isbn;
    private String editorial;
    private int cant_paginas;

    public Libro(String nombre, String autor, GregorianCalendar fecha_publicacion, String isbn, String editorial, int cant_paginas) {
        this.nombre = nombre;
        this.autor = autor;
        this.fecha_publicacion = fecha_publicacion;
        this.isbn = isbn;
        this.editorial = editorial;
        this.cant_paginas = cant_paginas;
    }

    public String toString() {  // como def __unicode__(self): de Python
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        // fecha_publicacion.getTime() Returns a Date object 
        String fecha = sdf.format(fecha_publicacion.getTime());
        return this.autor + " - " + this.editorial + " - " + fecha;
    }

    public static void main(String [] args) {
        Vector<Libro> vector = new Vector<Libro>();

        GregorianCalendar fecha_publicacion = new GregorianCalendar(1997, 5, 05);
        Libro libro = new Libro("Cien años de soledad", "Gabriel García Márquez", fecha_publicacion, "9789580600022", "Harper", 471);
        System.out.println(libro);
        vector.add(libro);

        fecha_publicacion = new GregorianCalendar(1873, 0, 30);
        libro = new Libro("La vuelta al mundo en 80 días", "Julio Verne", fecha_publicacion, "9780143034452", "Pierre-Jules Hetzel", 224);
        vector.add(libro);
        System.out.println(libro);

        for (Libro item : vector) {
            System.out.println(item);            
        }
    }
}
